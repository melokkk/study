package ru.specialist.spring.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.specialist.spring.dto.CommentDto;
import ru.specialist.spring.entity.Comment;
import ru.specialist.spring.entity.Post;
import ru.specialist.spring.repository.CommentRepository;
import ru.specialist.spring.repository.PostRepository;
import ru.specialist.spring.repository.UserRepository;

import java.time.LocalDateTime;

import static ru.specialist.spring.util.SecurityUtils.getCurrentUserDetails;

@Service
@Transactional
public class CommentService {

    private final UserRepository userRepository;
    private final CommentRepository commentRepository;
    private final PostRepository postRepository;


    @Autowired
    public CommentService(
            UserRepository userRepository, CommentRepository commentRepository, PostRepository postRepository) {
        this.userRepository = userRepository;
        this.commentRepository = commentRepository;
        this.postRepository = postRepository;
    }

    @PreAuthorize("hasRole('USER')")
    public Long create(CommentDto commentDto, Post post) {
        Comment comment = new Comment();
        comment.setContent(commentDto.getContent());
        comment.setPost(post);
        comment.setUser(userRepository.findByUsername(getCurrentUserDetails()
                        .getUsername())
                .orElseThrow());
        comment.setDtCreated(LocalDateTime.now());
        return commentRepository.save(comment).getCommentId();
    }

}
