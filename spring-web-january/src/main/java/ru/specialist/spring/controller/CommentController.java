package ru.specialist.spring.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import ru.specialist.spring.dto.CommentDto;
import ru.specialist.spring.entity.Post;
import ru.specialist.spring.service.CommentService;

@Controller
public class CommentController {

    private final CommentService commentService;

    @Autowired
    public CommentController(CommentService commentService) {
        this.commentService = commentService;
    }

    @PostMapping("/comment/create")
    @PreAuthorize("hasRole('USER')")
    public String commentCreate(CommentDto commentDto, Post post, ModelMap model){
        Long commentId = commentService.create(commentDto,post);
        Long postId = post.getPostId();
        return "redirect:/post/" + postId;
    }

}
